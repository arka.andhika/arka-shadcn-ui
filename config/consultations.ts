
export type Consultation = {
  id: number
  number: string
  title: string
  category?: string
  status: "active" | "close" | "ticketing" | "mou" | "invoicing"
  created: Date
  updated: Date
}

export async function getConsultations(): Promise<Consultation[]> {
  // Fetch data from your API here.
  return [
    {
      id: 123,
      number: "728ed53f",
      title: "testing 03",
      status: "active",
      created: new Date(),
      updated: new Date(),
    },
    {
      id: 124,
      number: "728ed54f",
      title: "testing 04",
      status: "close",
      created: new Date(),
      updated: new Date(),
    },
    {
      id: 125,
      number: "728ed55f",
      title: "testing 05",
      status: "ticketing",
      created: new Date(),
      updated: new Date(),
    },
    {
      id: 126,
      number: "728ed56f",
      title: "testing 06",
      status: "mou",
      created: new Date(),
      updated: new Date(),
    },
    {
      id: 127,
      number: "728ed57f",
      title: "testing 07",
      status: "invoicing",
      created: new Date(),
      updated: new Date(),
    },
    {
      id: 128,
      number: "728ed58f",
      title: "testing 08",
      status: "invoicing",
      created: new Date(),
      updated: new Date(),
    },
    {
      id: 129,
      number: "728ed59f",
      title: "testing 09",
      status: "invoicing",
      created: new Date(),
      updated: new Date(),
    },
    {
      id: 130,
      number: "728ed60f",
      title: "testing 10",
      status: "invoicing",
      created: new Date(),
      updated: new Date(),
    },
    {
      id: 131,
      number: "728ed61f",
      title: "testing 11",
      status: "invoicing",
      created: new Date(),
      updated: new Date(),
    },
    {
      id: 132,
      number: "728ed62f",
      title: "testing 12",
      status: "invoicing",
      created: new Date(),
      updated: new Date(),
    },
    {
      id: 133,
      number: "728ed63f",
      title: "testing 13",
      status: "invoicing",
      created: new Date(),
      updated: new Date(),
    },
    {
      id: 134,
      number: "728ed64f",
      title: "testing 14",
      status: "invoicing",
      created: new Date(),
      updated: new Date(),
    },
    {
      id: 135,
      number: "728ed65f",
      title: "testing 15",
      status: "invoicing",
      created: new Date(),
      updated: new Date(),
    },
    {
      id: 136,
      number: "728ed66f",
      title: "testing 16",
      status: "invoicing",
      created: new Date(),
      updated: new Date(),
    },
    {
      id: 137,
      number: "728ed67f",
      title: "testing 17",
      status: "invoicing",
      created: new Date(),
      updated: new Date(),
    },
    {
      id: 138,
      number: "728ed68f",
      title: "testing 18",
      status: "invoicing",
      created: new Date(),
      updated: new Date(),
    },
    // ...
  ]
}