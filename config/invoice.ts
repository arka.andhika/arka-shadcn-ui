
export interface invoice {
  id: number
  userid: number
  number: string
  items: invoiceitem[]
  status: "unpaid" | "paid" | "canceled" | "refund"
  subtotal: number
  discount?: number
  tax: number
  total: number
  balance: number
  createdat: Date
  updatedat: Date
  duedate: Date
}

export interface invoiceitem {
  id: number
  relate?: "service" | "addon"
  relid?: number
  desc: string
  amount: number
  discount?: number
  tax: number
}

interface transaction {
  id: number
  invoiceid: number
  number: string
  gateway: string
  amount: number
  createdat: Date
  updatedat: Date
}

const dummyInvoices : invoice[] = [
  {
    id: 1,
    userid: 1,
    number: "asdlkqwe1",
    items: [
      {
        id: 1,
        relate: "service",
        relid: 4,
        desc: "Order project service",
        amount: 10000000,
        tax: 1000000
      }
    ],
    status: "paid",
    subtotal: 10000000,
    tax: 1000000,
    total: 11000000,
    balance: 0,
    createdat: new Date(),
    updatedat: new Date(),
    duedate: new Date()
  },
  {
    id: 2,
    userid: 1,
    number: "asdlkqwe2",
    items: [
      {
        id: 1,
        relate: "addon",
        relid: 1,
        desc: "Order addon 01/09/2023 - 01/10/2023",
        amount: 1000000,
        tax: 1000000
      }
    ],
    status: "unpaid",
    subtotal: 1000000,
    tax: 100000,
    total: 1100000,
    balance: 1100000,
    createdat: new Date(),
    updatedat: new Date(),
    duedate: new Date()
  },
  {
    id: 3,
    userid: 1,
    number: "asdlkqwe3",
    items: [
      {
        id: 1,
        relate: "service",
        relid: 1,
        desc: "Order addon 01/09/2023 - 01/09/2024",
        amount: 1000000,
        tax: 100000
      }
    ],
    status: "unpaid",
    subtotal: 1000000,
    tax: 100000,
    total: 1100000,
    balance: 1100000,
    createdat: new Date(),
    updatedat: new Date(),
    duedate: new Date()
  },
  {
    id: 4,
    userid: 1,
    number: "asdlkqwe4",
    items: [
      {
        id: 1,
        relate: "service",
        relid: 2,
        desc: "Order addon 01/09/2023 - 01/10/2023",
        amount: 1000000,
        tax: 100000
      }
    ],
    status: "paid",
    subtotal: 1000000,
    tax: 100000,
    total: 1100000,
    balance: 0,
    createdat: new Date(),
    updatedat: new Date(),
    duedate: new Date()
  },
  {
    id: 5,
    userid: 1,
    number: "asdlkqwe5",
    items: [
      {
        id: 1,
        relate: "service",
        relid: 3,
        desc: "Order addon 01/09/2023 - 01/10/2023",
        amount: 5000000,
        tax: 500000
      }
    ],
    status: "paid",
    subtotal: 5000000,
    tax: 500000,
    total: 5500000,
    balance: 0,
    createdat: new Date(),
    updatedat: new Date(),
    duedate: new Date()
  }
];

const dummyTrans: transaction[] = [
  {
    id: 1,
    invoiceid: 1,
    number: "1231231201",
    gateway: "BCA",
    amount: 11000000,
    createdat: new Date,
    updatedat: new Date
  },
  {
    id: 2,
    invoiceid: 4,
    number: "1231231202",
    gateway: "BCA",
    amount: 1100000,
    createdat: new Date,
    updatedat: new Date
  },
  {
    id: 3,
    invoiceid: 5,
    number: "1231231203",
    gateway: "BCA",
    amount: 5500000,
    createdat: new Date,
    updatedat: new Date
  },
]

export async function getInvoices(): Promise<invoice[]> {
  return dummyInvoices;
}

export async function detailInvoice(no: string): Promise<invoice | null> {
  const invoice = dummyInvoices.filter(invoice => invoice.number === no);

  if (!invoice.length)
    return null

  return invoice[0];
}

interface getInvoiceByRelidProp {
  relate: "service" | "addon"
  relid: number
}

export async function getInvoiceByRelid({ relate, relid }: getInvoiceByRelidProp): Promise<invoice[] | null> {
  const invoice = dummyInvoices.filter(invoice => {
    const items = invoice.items.filter(item => item.relate === relate && item.relid === relid);
    return items.length > 0;
  });

  if (!invoice.length) 
    return null;

  return invoice
}

export async function getTransByInvoiceId({ invoiceid }: { invoiceid: number }): Promise<transaction[] | null> {
  const trans = dummyTrans.filter(trans => trans.invoiceid === invoiceid);

  if (!trans.length)
    return null

  return trans;
}