export type SiteConfig = typeof siteConfig

export const siteConfig = {
  name: "okumaru",
  description:
    "Beautifully designed components built with Radix UI and Tailwind CSS.",
  mainNav: [
    {
      title: "Home",
      href: "/",
    },
    {
      title: "Services",
      href: "/",
    },
  ],
  links: {
    twitter: "https://twitter.com/shadcn",
    github: "https://github.com/shadcn/ui",
    docs: "https://ui.shadcn.com",
  },
  quote: {
    text: 'We made for Human by Human and to be Human with digitalization.',
  }
}
