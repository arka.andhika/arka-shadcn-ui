import { Ticket } from "lucide-react"

export interface ticket {
  id: number
  userid: number
  relate?: "service" | "addon"
  relid?: number
  number: string
  title: string
  desc: string
  status: "open" | "reply" | "hold" | "close"
  replys?: reply[]
  createdat: Date
  updatedat: Date
}

export interface reply {
  id: number
  ticketid: number
  content: string
  userid?: number
  adminid?: number
  createdat: Date
  updatedat: Date
}

const dummyTickets: ticket[] = [
  {
    id: 1,
    userid: 1,
    number: "asdlkqwe1",
    title: "lorem ipsum 1 ekodomo 1",
    desc: "lorem ipsum 1 ekodomo 1",
    status: "open",
    createdat: new Date(),
    updatedat: new Date(),
    replys: [],
  },
  {
    id: 2,
    userid: 1,
    relate: "service",
    relid: 4,
    number: "asdlkqwe2",
    title: "lorem ipsum 2 ekodomo 1",
    desc: "Hi! thank your for use our platform for communicating with Us. Also please continue use this platform as main activity to Us.",
    status: "reply",
    createdat: new Date(),
    updatedat: new Date(),
    replys: [
      {
        id: 1,
        ticketid: 2,
        content: "asdasd",
        adminid: 1,
        createdat: new Date(),
        updatedat: new Date()
      },
      {
        id: 2,
        ticketid: 2,
        content: "asdasd",
        userid: 1,
        createdat: new Date(),
        updatedat: new Date()
      },
      {
        id: 3,
        ticketid: 2,
        content: "asdasd",
        userid: 1,
        createdat: new Date(),
        updatedat: new Date()
      },
      {
        id: 4,
        ticketid: 2,
        content: "asdasd",
        adminid: 1,
        createdat: new Date(),
        updatedat: new Date()
      },
    ]
  },
  {
    id: 1,
    userid: 1,
    number: "asdlkqwe3",
    title: "lorem ipsum 1 ekodomo 2",
    desc: "lorem ipsum 1 ekodomo 2",
    status: "open",
    createdat: new Date(),
    updatedat: new Date(),
    replys: [],
  },
]

export async function detailTicket(number: string) : Promise<ticket | null> {
  const ticket = dummyTickets.filter(ticket => ticket.number === number);

  if (!ticket.length) 
    return null;

  return ticket[0]
}

export async function getTickets() : Promise<ticket[]> {
  return dummyTickets
}

interface getTicketByRelid {
  relate: "service" | "addon"
  relid: number
}

export async function getTicketByRelid({ relate, relid }: getTicketByRelid) : Promise<ticket[] | null> {
  const ticket = dummyTickets.filter(tic => tic.relate === relate && tic.relid === relid);

  if (!ticket.length)
    return null;

  return ticket;
}