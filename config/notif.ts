export interface Notif {
  id: number
  title: string
  slug: string
  desc: string
  date: Date
  category: string
  opened: boolean
  link: string
  userid: number|null
}

export const listNotif: Notif[] = [
  {
    id: 1,
    title: "React Rendezvous",
    slug: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",
    desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",
    date: new Date(),
    category: 'Pembayaran',
    opened: false,
    link: "https://images.unsplash.com/photo-1611348586804-61bf6c080437?w=300&dpr=2&q=80",
    userid: 1,
  },
  {
    id: 2,
    title: "Async Awakenings",
    slug: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",
    desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",
    category: 'Coupon',
    date: new Date(),
    opened: false,
    link: "https://images.unsplash.com/photo-1468817814611-b7edf94b5d60?w=300&dpr=2&q=80",
    userid: null,
  },
  {
    id: 3,
    title: "The Art of Reusability",
    slug: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",
    desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",
    category: 'Order',
    date: new Date(),
    opened: true,
    link: "https://images.unsplash.com/photo-1528143358888-6d3c7f67bd5d?w=300&dpr=2&q=80",
    userid: null,
  },
  {
    id: 4,
    title: "The Art of Reusability",
    slug: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",
    desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",
    category: 'Order',
    date: new Date(),
    opened: true,
    link: "https://images.unsplash.com/photo-1528143358888-6d3c7f67bd5d?w=300&dpr=2&q=80",
    userid: null,
  },
]