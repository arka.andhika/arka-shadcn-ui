export interface Activity {
  name: string
  date: Date
  userid: number | null
}

export const listActivities: Activity[] = [
  {
    name: "React Rendezvous",
    date: new Date(),
    userid: 1,
  },
  {
    name: "Async Awakenings",
    date: new Date(),
    userid: null,
  },
  {
    name: "The Art of Reusability",
    date: new Date(),
    userid: null,
  },
  {
    name: "The Art of Reusability",
    date: new Date(),
    userid: null,
  },
]