

import * as React from "react"
import { Metadata } from "next";

import { Separator } from "@/components/ui/separator";
import {
  Accordion,
  AccordionContent,
  AccordionItem,
  AccordionTrigger,
} from "@/components/ui/accordion"

import { BedgeDate } from "./components/bedge-date";
import { BedgeTime } from "./components/bedge-time";
import { BedgeOpened } from "./components/bedge-opened";
import { ButtonActions } from "./components/button-actions";
import "./styles.css"

import { siteConfig } from "@/config/site";
import { groupNotif } from "./data/notification";

export const metadata: Metadata = {
  title: {
    default: "Notifications",
    template: `%s - ${siteConfig.name}`,
  },
  description: "Notification page of STR o'House website to you.",
}

export default async function NotifPage() {

  return (
    <>
      <div className="space-between space-y-4">
        <h2 className="text-3xl font-bold tracking-tight">Notification</h2>
      </div>

      <Separator className="my-4" />

      <div className="flex flex-col gap-y-5">
        {groupNotif?.map((notif, i) => {
          return <div key={i} className="flex flex-col space-y-4">
            <div><BedgeDate date={notif.date} /></div>
            <div className="p-6 rounded-lg border bg-card text-card-foreground shadow-sm">
              <Accordion type="single" collapsible>
                {notif.list.map((item, i2) => {
                  return <AccordionItem key={i2} value={`item-${i2}`} className="border-b-0">
                    <AccordionTrigger>
                      <div className="flex flex-wrap space-x-4 gap-y-4 items-center">
                        <BedgeTime date={item.date} />
                        <BedgeOpened opened={item.opened} />
                        <p>{item.title}</p>
                      </div>
                    </AccordionTrigger>
                    <AccordionContent>
                      <div className="mb-2 flex flex-wrap gap-2">
                        <ButtonActions item={item} />
                      </div>
                      <p className="text-muted-foreground">{item.slug}</p>
                    </AccordionContent>
                  </AccordionItem>
                })}
              </Accordion>
            </div>
          </div>
        })}
      </div>
    </>
  )
}