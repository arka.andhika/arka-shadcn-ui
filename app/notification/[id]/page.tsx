

import * as React from "react"
import { Metadata } from "next";
import Link from "next/link";

import { Separator } from "@/components/ui/separator";
import { Button } from "@/components/ui/button";
import { TooltipProvider } from "@/components/ui/tooltip"

import { BedgeCategory } from "./components/bedge-category";
import { BedgeDate } from "./components/bedge-date";
import { BedgeOpened } from "./components/bedge-opened";
import { TooltipBack } from "./components/tooltip-back";
import { TooltipUnread } from "./components/tooltip-unread";
import { TooltipDelete } from "./components/tooltip-delete";
import "../styles.css"

import { siteConfig } from "@/config/site";
import { Notif, listNotif } from "@/config/notif";

export const metadata: Metadata = {
  title: {
    default: "Detail Notifications",
    template: `%s - ${siteConfig.name}`,
  },
  description: "Detail notification page of STR o'House website to you.",
}

export default async function NotifPage({ params }: { params: { id: number } }) {

  const dataNotif: Notif[] = listNotif.filter(notif => notif.id == params.id);
  const detailNotif: Notif = dataNotif[0];

  if (dataNotif.length > 1 || dataNotif.length === 0)
    return <div className="flex flex-col justify-center items-center space-y-4">
      <p className="text-xl font-bold">Oops, notification not found!</p>
      <Link href={`/notification`}><Button>Go back</Button></Link>
    </div>

  return (
    <>
      <div className="space-between space-y-4">
        <h2 className="text-3xl font-bold tracking-tight">{detailNotif.title}</h2>
        <div className="flex flex-wrap gap-x-4">
          <BedgeDate date={detailNotif.date} />
          <BedgeCategory category={detailNotif.category} />
          <BedgeOpened opened={detailNotif.opened} />
        </div>
        <div>
          <p className="text-sm text-muted-foreground">{detailNotif.slug}</p>
        </div>
      </div>

      <Separator className="my-4" />

      <div className="flex flex-col gap-y-5">
        <div>
          <div className="mb-2 flex flex-wrap gap-2">
            <TooltipProvider>
              <TooltipBack />
              <TooltipUnread />
              <TooltipDelete />
            </TooltipProvider>
          </div>
        </div>
        <div>
          {detailNotif.desc}
        </div>
      </div>
    </>
  )
}