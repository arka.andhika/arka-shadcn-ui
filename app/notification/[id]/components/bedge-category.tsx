
import { Badge } from "@/components/ui/badge"

interface bedge {
  category: string
}

export const BedgeCategory = ({ category }: bedge) => {
  return <Badge variant="secondary" className="mb-5 py-2 px-5">
    {category}
  </Badge>
}