
import Link from "next/link"

import { StepBack } from "lucide-react"
import { Button } from "@/components/ui/button"
import {
  Tooltip,
  TooltipContent,
  TooltipTrigger,
} from "@/components/ui/tooltip"

export const TooltipBack = () => {
  return <Tooltip>
    <TooltipTrigger asChild>
      <Link href={`/notification`}>
        <Button variant={`outline`} className="text-sm">
          <StepBack className="h-4 w-4" />
        </Button>
      </Link>
    </TooltipTrigger>
    <TooltipContent>
      <p className="text-primary">Back to list notification</p>
    </TooltipContent>
  </Tooltip>
}