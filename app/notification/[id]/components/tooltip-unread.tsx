
import { MailQuestion } from "lucide-react"
import { Button } from "@/components/ui/button"
import {
  Tooltip,
  TooltipContent,
  TooltipTrigger,
} from "@/components/ui/tooltip"

export const TooltipUnread = () => {
  return <Tooltip>
    <TooltipTrigger asChild>
      <Button variant={`outline`} className="text-sm">
        <MailQuestion className="h-4 w-4" />
      </Button>
    </TooltipTrigger>
    <TooltipContent>
      <p className="text-primary">Mark unread notification</p>
    </TooltipContent>
  </Tooltip>
}