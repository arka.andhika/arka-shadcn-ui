
import { MailX } from "lucide-react"
import { Button } from "@/components/ui/button"
import {
  Tooltip,
  TooltipContent,
  TooltipTrigger,
} from "@/components/ui/tooltip"

export const TooltipDelete = () => {
  return <Tooltip>
    <TooltipTrigger asChild>
      <Button variant={`destructive`} className="text-sm">
        <MailX className="h-4 w-4" />
      </Button>
    </TooltipTrigger>
    <TooltipContent>
      <p className="text-primary">Delete notification</p>
    </TooltipContent>
  </Tooltip>
}