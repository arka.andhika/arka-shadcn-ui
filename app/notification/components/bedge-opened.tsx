
import { Badge } from "@/components/ui/badge"

interface opended {
  opened: boolean
}

export const BedgeOpened = ({ opened }: opended) => {
  return <Badge variant="secondary" className="py-2 px-5">
    {opened ? 'read' : 'unread'}
  </Badge>
}