
import { Badge } from "@/components/ui/badge"

import monthToString from "@/lib/month-to-string";

const dtToTime = (date: Date) => {
  return `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`
}

interface date {
  date: Date
}

export const BedgeDate = ({ date }: date) => {
  return <Badge variant="secondary" className="py-2 px-5">
    {monthToString(date.getMonth())} {" "}
    {(date.getDate()).toString()}
  </Badge>
}