
import Link from "next/link";

import { Send, MailOpen, MailQuestion, MailX } from "lucide-react"
import { Button } from "@/components/ui/button";

import { Notif } from "@/config/notif";

interface actions {
  item: Notif
}

export const ButtonActions = ({ item }: actions) => {
  return <>
    <Link href={`/notification/${item.id}`}>
      <Button className="text-sm">
        <Send className="mr-2 h-4 w-4" />
        Open Detail
      </Button>
    </Link>
    <Button variant={`secondary`} className="text-sm">
      <MailOpen className="mr-2 h-4 w-4" />
      Mark as Read
    </Button>
    <Button variant={`secondary`} className="text-sm">
      <MailQuestion className="mr-2 h-4 w-4" />
      Mark as Unread
    </Button>
    <Button variant={`destructive`} className="text-sm">
      <MailX className="mr-2 h-4 w-4" />
      Delete
    </Button>
  </>
}