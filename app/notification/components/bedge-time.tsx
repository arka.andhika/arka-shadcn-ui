
import { Badge } from "@/components/ui/badge"

import monthToString from "@/lib/month-to-string";

const dtToTime = (date: Date) => {
  return `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`
}

interface date {
  date: Date
}

export const BedgeTime = ({ date }: date) => {
  return <Badge variant="outline" className="py-2 px-5">
    {dtToTime(date)}
  </Badge>
}