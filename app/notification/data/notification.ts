import { Notif } from "@/config/notif"

export interface GroupNotif {
  date: Date,
  list: Notif[]
}

export const groupNotif: GroupNotif[] = [
  {
    date: new Date('2023-06-01'),
    list: [
      {
        id: 1,
        title: "React Rendezvous",
        slug: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",
        desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",
        date: new Date('2023-06-01 10:11:23'),
        category: 'Pembayaran',
        opened: false,
        link: "https://images.unsplash.com/photo-1611348586804-61bf6c080437?w=300&dpr=2&q=80",
        userid: 1,
      },
      {
        id: 2,
        title: "Async Awakenings",
        slug: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",
        desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",
        date: new Date('2023-06-01 10:12:27'),
        category: 'Coupon',
        opened: false,
        link: "https://images.unsplash.com/photo-1468817814611-b7edf94b5d60?w=300&dpr=2&q=80",
        userid: null,
      },
    ]
  },
  {
    date: new Date('2023-06-02'),
    list: [
      {
        id: 3,
        title: "React Rendezvous",
        slug: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",
        desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",
        date: new Date('2023-06-01 10:11:23'),
        category: 'Pembayaran',
        opened: false,
        link: "https://images.unsplash.com/photo-1611348586804-61bf6c080437?w=300&dpr=2&q=80",
        userid: 1,
      },
      {
        id: 4,
        title: "Async Awakenings",
        slug: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",
        desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",
        date: new Date('2023-06-01 10:12:27'),
        category: 'Coupon',
        opened: false,
        link: "https://images.unsplash.com/photo-1468817814611-b7edf94b5d60?w=300&dpr=2&q=80",
        userid: null,
      },
    ]
  },
  {
    date: new Date('2023-06-03'),
    list: [
      {
        id: 5,
        title: "React Rendezvous",
        slug: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",
        desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",
        date: new Date('2023-06-01 10:11:23'),
        category: 'Pembayaran',
        opened: false,
        link: "https://images.unsplash.com/photo-1611348586804-61bf6c080437?w=300&dpr=2&q=80",
        userid: 1,
      },
      {
        id: 6,
        title: "Async Awakenings",
        slug: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",
        desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",
        date: new Date('2023-06-01 10:12:27'),
        category: 'Coupon',
        opened: false,
        link: "https://images.unsplash.com/photo-1468817814611-b7edf94b5d60?w=300&dpr=2&q=80",
        userid: null,
      },
    ]
  }
]