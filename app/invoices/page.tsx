import * as React from "react"
import { Metadata } from "next";

import { Separator } from "@/components/ui/separator";

import { DataTableInvoice } from "@/components/invoices/table-data";
import { columnsInvoices } from "@/components/invoices/table-columns";

import "./styles.css"

import { siteConfig } from "@/config/site"
import { getInvoices } from "@/config/invoice";

export const metadata: Metadata = {
  title: {
    default: "Invoices",
    template: `%s - ${siteConfig.name}`,
  },
  description: "Invoices page of STR o'House website.",
}

export default async function InvoicesPage() {
  const invoices = await getInvoices();

  return (
    <>
      <div className="space-between space-y-4">
        <h2 className="text-3xl font-bold tracking-tight">Invoices</h2>
        <p className="text-sm text-muted-foreground text-center sm:text-left">
          Your list data of invoice to Us.
        </p>
      </div>
      
      <Separator className="my-4" />

      <DataTableInvoice columns={columnsInvoices} data={invoices} />
    </>
  )
}