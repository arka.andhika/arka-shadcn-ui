
import { Separator } from "@/components/ui/separator"
import {
  Card,
  CardContent,
  CardHeader,
  CardTitle,
} from "@/components/ui/card"

import { getTransByInvoiceId } from "@/config/invoice";
import { IDRupiah } from "@/lib/currency";
import { dateTime } from "@/lib/date-time";

async function InvoiceTrans ({ invId, invBalance }: { invId: number, invBalance: number }) {
  const trans = await getTransByInvoiceId({ invoiceid: invId });

  return <Card className="px-0">
    <CardHeader>
      <CardTitle>Transaction</CardTitle>
    </CardHeader>
    <Separator className="hidden md:block" />
    <div className="hidden md:grid grid-cols-1 md:grid-cols-5 gap-x-4 p-6 text-sm">
      <div className="md:col-span-2">No. Trans</div>
      <div>Gateway</div>
      <div>Date</div>
      <div>Amount</div>
    </div>
    {trans && trans?.map(trans => {
      return <>
        <Separator />
        <div className="grid md:grid-cols-5 gap-x-4 items-center px-6 py-4 text-sm text-muted-foreground">
          <div className="md:col-span-2 text-primary">#{trans.number}</div>
          <div>
            <p>{trans.gateway}</p>
          </div>
          <div>
            <p>{dateTime(trans.createdat)}</p>
          </div>
          <div>
            <p>{IDRupiah(trans.amount)}</p>
          </div>
        </div>
      </>
    })}

    <Separator />
    <div className="grid md:grid-cols-5 gap-x-4 items-center px-6 py-4 text-sm text-muted-foreground">
      <div className="md:col-span-4 text-primary md:text-right">Balance</div>
      <div>
        <p>{IDRupiah(invBalance)}</p>
      </div>
    </div>
  </Card>
}

export { InvoiceTrans }