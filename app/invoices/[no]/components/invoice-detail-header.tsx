
import { Button } from "@/components/ui/button";

import { BadgeDateSmall } from "@/components/badge/badge-date";

import { invoice } from "@/config/invoice"
import { IDRupiah } from "@/lib/currency";

const ButtonStatus = ({status}: {status: "unpaid" | "paid" | "canceled" | "refund"}) => {
  let button;
  switch(status) {
    case "paid":
      button = <Button variant={"outline"}>{status.toUpperCase()}</Button>
      break;
    case "canceled":
      button = <Button variant={"secondary"}>{status.toUpperCase()}</Button>
      break;
    case "refund":
      button = <Button>{status.toUpperCase()}</Button>
      break;
    default:
      button = <Button variant={"destructive"}>{status.toUpperCase()}</Button>
  }

  return button;
}

export const InvDetailHeader = ({ invoice }: { invoice: invoice }) => {
  return <div className="flex flex-col lg:flex-row lg:items-center lg:justify-between space-y-6 lg:space-y-0">
    <div className="space-y-2">
      <div className="flex items-center justify-start gap-4">
        <h2 className="text-3xl font-bold tracking-tight text-center sm:text-left">
          <span className="font-light">Invoice</span>{" "}
          #{invoice.number.toUpperCase()}
        </h2>
        <ButtonStatus status={invoice.status} />
      </div>
      <div className="text-sm space-y-2">
        <div>
          <span className="text-muted-foreground mr-4">Invoice Created:</span> 
          <BadgeDateSmall date={invoice.createdat} />
        </div>
        <div>
          <span className="text-muted-foreground mr-4">Invoice Due Date:</span> 
          <BadgeDateSmall date={invoice.duedate} />
        </div>
      </div>
    </div>
    <div>
      <p className="text-muted-foreground mr-4">Total</p>
      <h1 className="text-3xl font-bold tracking-tight text-center sm:text-left">{IDRupiah(invoice.total)}</h1>
    </div>
  </div>
}