
import { Separator } from "@/components/ui/separator"
import {
  Card,
  CardContent,
  CardHeader,
  CardTitle,
} from "@/components/ui/card"

import { invoice, invoiceitem } from "@/config/invoice"
import { IDRupiah } from "@/lib/currency"

export const InvoiceItems = ({ invoice }: { invoice: invoice }) => {
  return <Card className="px-0">
  <CardHeader>
    <CardTitle>Invoice Items</CardTitle>
  </CardHeader>
  <Separator className="hidden md:block" />
  <div className="hidden md:grid md:grid-cols-3 gap-x-4 p-6 text-sm">
    <div className="col-span-2">Description</div>
    <div>Amount</div>
  </div>
  {invoice.items.map(item => {
    return <>
      <Separator />
      <div className="grid md:grid-cols-3 gap-x-4 items-center px-6 py-4">
        <div className="col-span-2">{item.desc}</div>
        <div className="text-sm text-muted-foreground">
          <p>{IDRupiah(item.amount)}</p>
          <p>Disc: {IDRupiah(item.discount ?? 0)}</p>
          <p>Tax: {IDRupiah(item.tax)}</p>
        </div>
      </div>
    </>
  })}

  <Separator />
  <div className="py-2 space-y-2">
    <div className="grid md:grid-cols-3 gap-x-4 items-center px-6 text-sm">
      <div className="col-span-2 md:text-right">Subtotal</div>
      <div className="text-muted-foreground">
        <p>{IDRupiah(invoice.subtotal)}</p>
      </div>
    </div>
    <div className="grid md:grid-cols-3 gap-x-4 items-center px-6 text-sm">
      <div className="col-span-2 md:text-right">Discount</div>
      <div className="text-muted-foreground">
        <p>{IDRupiah(invoice.discount ?? 0)}</p>
      </div>
    </div>
    <div className="grid md:grid-cols-3 gap-x-4 items-center px-6 text-sm">
      <div className="col-span-2 md:text-right">Tax</div>
      <div className="text-muted-foreground">
        <p>{IDRupiah(invoice.tax)}</p>
      </div>
    </div>
    <div className="grid md:grid-cols-3 gap-x-4 items-center px-6 text-sm">
      <div className="col-span-2 md:text-right">Total</div>
      <div className="text-muted-foreground">
        <p>{IDRupiah(invoice.total)}</p>
      </div>
    </div>
  </div>
</Card>
}