
import { Separator } from "@/components/ui/separator"
import {
  Card,
  CardContent,
  CardHeader,
  CardTitle,
} from "@/components/ui/card"

async function InvoiceAppoint() {
  return <Card className="grid md:grid-cols-2 gap-x-4">
    <div>
      <CardHeader>
        <CardTitle>Pay To:</CardTitle>
      </CardHeader>
      <CardContent className="text-sm text-muted-foreground">
        <p>Andhika M. Wijaya</p>
        <p>Address</p>
        <p>Email</p>
        <p>Phone</p>
      </CardContent>
    </div>

    <Separator className="md:hidden" />

    <div>
      <CardHeader>
        <CardTitle>Invoice To:</CardTitle>
      </CardHeader>
      <CardContent className="text-sm text-muted-foreground">
        <p>Asdasdjlj laskjdlqwe</p>
        <p>Address</p>
        <p>Email</p>
        <p>Phone</p>
      </CardContent>
    </div>
  </Card>
}
export { InvoiceAppoint }