import * as React from "react"
import { Metadata } from "next";
import Link from "next/link";

import { Button } from "@/components/ui/button";
import { Separator } from "@/components/ui/separator";

import { Breadcrumbs } from "@/components/breadcrumbs";
import { InvDetailHeader } from "./components/invoice-detail-header";
import { InvoiceAppoint } from "./components/invoice-appoint";
import { InvoiceItems } from "./components/invoice-item";
import { InvoiceTrans } from "./components/invoice-tran";
import "../styles.css"

import { siteConfig } from "@/config/site"
import { detailInvoice } from "@/config/invoice"
import { getTransByInvoiceId } from "@/config/invoice";

export const metadata: Metadata = {
  title: {
    default: "Detail Invoice",
    template: `%s - ${siteConfig.name}`,
  },
  description: "Detail your invoice to STR o'House.",
}

export default async function DetailInvoicePage({ params }: { params: { no: string } }) {
  const invoice = await detailInvoice(params.no);
  const trans = await getTransByInvoiceId({ invoiceid: invoice?.id ?? 0 });

  if (!invoice)
    return <div className="flex flex-col justify-center items-center space-y-4">
      <p className="text-xl font-bold">Oops, invoice not found!</p>
      <Link href={`/invoices`}><Button>Go back</Button></Link>
    </div>

  return <>
    <Breadcrumbs items={["Invoice", "Detail"]} />

    <div className="space-between space-y-4">
      <InvDetailHeader invoice={invoice} />
      <Separator />
      <InvoiceAppoint />
      <InvoiceItems invoice={invoice} />
      <InvoiceTrans invId={invoice.id} invBalance={invoice.balance} />
    </div>
  </>
}