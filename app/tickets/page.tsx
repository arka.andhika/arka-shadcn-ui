import * as React from "react"
import { Metadata } from "next";
import Link from "next/link";

import { PlusCircle } from "lucide-react"
import { Button } from "@/components/ui/button";
import { Separator } from "@/components/ui/separator";

import { DataTable } from "./components/table-data";
import { columns } from "./components/table-columns";
import "./styles.css"

import { siteConfig } from "@/config/site"
import { getTickets } from "@/config/ticket";

export const metadata: Metadata = {
  title: {
    default: "Tickets",
    template: `%s - ${siteConfig.name}`,
  },
  description: "Ticket page of STR o'House website.",
}

export default async function TicketPage() {
  const data = await getTickets();

  return (
    <>
      <div className="space-between space-y-4">
        <div className="space-between flex flex-col-reverse sm:flex-row items-center gap-y-4">
          <h2 className="text-3xl font-bold tracking-tight">List Tickets</h2>
          <div className="sm:ml-auto sm:mr-4">
            <Link href={'/tickets/new'}>
              <Button>
                <PlusCircle className="mr-2 h-4 w-4" />
                New Ticket
              </Button>
            </Link>
          </div>
        </div>
        <p className="text-sm text-muted-foreground text-center sm:text-left">
          Your list data of ticket to Us.
        </p>
      </div>
      
      <Separator className="my-4" />
      
      <DataTable columns={columns} data={data} />
    </>
  )
}