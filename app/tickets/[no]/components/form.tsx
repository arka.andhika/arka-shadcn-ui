"use client"

import * as React from "react"
import { zodResolver } from "@hookform/resolvers/zod"
import { useForm } from "react-hook-form"
import * as z from "zod"

import { Button } from "@/components/ui/button"
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormMessage,
} from "@/components/ui/form"
import { Textarea } from "@/components/ui/textarea"

const formSchema = z.object({
  content: z.string(),
})

export const FormReplyTicket = () => {

  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      content: "",
    },
  })

  // 2. Define a submit handler.
  function onSubmit(values: z.infer<typeof formSchema>) {
    // Do something with the form values.
    // ✅ This will be type-safe and validated.
    console.log(values)
  }

  return <Form {...form}>
    <form 
      onSubmit={form.handleSubmit(onSubmit)} 
      className="space-y-2 bg-secondary p-2 border rounded-lg"
    >
      <FormField
        control={form.control}
        name="content"
        render={({ field }) => (
          <FormItem>
            <FormControl>
              <Textarea {...field} className="bg-popover" placeholder="Please tell use more about detail information so we can understand the problem." />
            </FormControl>
            <FormMessage />
          </FormItem>
        )}
      />
      <div className="flex justify-end gap-x-2">
        <Button type="submit" className="px-8">Reply</Button>
        <Button type="submit" className="px-8 bg-destructive text-white hover:text-secondary">Close</Button>
      </div>
    </form>
  </Form>
}