
import { User } from "lucide-react"
import { Badge } from "@/components/ui/badge";
import { Avatar, AvatarFallback, AvatarImage } from "@/components/ui/avatar"

import { reply } from "@/config/ticket";
import monthToString from "@/lib/month-to-string";
import dtToTime from "@/lib/timestamp-to-time";

export const ReplyTicket = ({ reply }: { reply: reply }) => {
  return <div className="flex gap-x-4">
    <div className="flex-none">
      <Avatar>
        <AvatarImage src="https://github.com/okumaru.png" alt="@shadcn" />
        <AvatarFallback>
          <User />
          <span className="sr-only">User</span>
        </AvatarFallback>
      </Avatar>
    </div>
    <div className="flex-1 border rounded-md">
      <div className="border-b px-4 py-2 flex justify-between items-center">
        <p className="text-xs">
          {monthToString(reply.createdat.getMonth())} {" "}
          {(reply.createdat.getDate()).toString()} {" "}
          {dtToTime(reply.createdat)}
        </p>
        <div>
          {reply.adminid && <Badge variant="outline">admin</Badge>}
          {reply.userid && <Badge variant="outline">user</Badge>}
        </div>
      </div>
      <div className="p-4 text-sm">
        {reply.content}
      </div>
    </div>
  </div>
}