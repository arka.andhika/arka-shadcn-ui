import * as React from "react"
import { Metadata } from "next";
import Link from "next/link";

import { Button } from "@/components/ui/button";
import { Separator } from "@/components/ui/separator";
import { Badge } from "@/components/ui/badge";

import { BadgeDate } from "@/components/badge/badge-date";
import { ReplyTicket } from "./components/reply";
import { FormReplyTicket } from "./components/form";
import "../styles.css"

import { siteConfig } from "@/config/site"
import { detailTicket } from "@/config/ticket"

export const metadata: Metadata = {
  title: {
    default: "Detail Ticket",
    template: `%s - ${siteConfig.name}`,
  },
  description: "Detail your ticket to STR o'House.",
}

export default async function DetailTicketPage({ params }: { params: { no: string } }) {

  const ticket = await detailTicket(params.no);

  if (!ticket)
    return <div className="flex flex-col justify-center items-center space-y-4">
      <p className="text-xl font-bold">Oops, ticket not found!</p>
      <Link href={`/tickets`}><Button>Go back</Button></Link>
    </div>

  return (
    <>
      <div className="space-between space-y-4">
        <div>
          <div className="flex items-center justify-center sm:justify-start gap-x-2">
            <p className="text-xl tracking-tight">Detail Ticket</p>
            <Badge>#{ticket?.number}</Badge>
          </div>
          <h2 className="text-3xl font-bold tracking-tight text-center sm:text-left">{ticket?.title}</h2>
        </div>
        <div className="flex flex-wrap gap-x-2">
          <Badge variant="secondary" className="mb-5 py-2 px-5">{ticket?.status}</Badge>
          <BadgeDate date={ticket?.createdat} label="Created" />
          <BadgeDate date={ticket?.updatedat} label="Updated" />
        </div>

        <div className="text-sm text-muted-foreground p-4 border rounded-md">
          {ticket?.desc}
        </div>
      </div>

      <Separator className="my-4" />
      
      <div className="flex flex-col content-center space-y-4">

        {ticket?.replys?.map((reply, index) => {
          return <ReplyTicket key={index} reply={reply} />
        })}

        <FormReplyTicket />
        
      </div>
    </>
  )
}