"use client"

import "@/styles/globals.css"
import * as React from "react"

import { SiteHeader } from "@/components/site-header"
import { Sidebar } from "@/components/sidebar"

interface ProductLayoutProps {
  children: React.ReactNode,
}

export default function ProductLayout({ children }: ProductLayoutProps) {

  const [marginLeft, setMarginLeft] = React.useState<string>('0px 0px 0px 213px');
  const [toggle, doToggle] = React.useState<boolean>(true);

  React.useEffect(() => {
    setMarginLeft(toggle ? '0px 0px 0px 213px' : '0px 0px 0px 76px');
  }, [toggle]);

  return <div className="relative flex min-h-screen flex-col">
    <div className="flex-1">
      <div className="block">
        <SiteHeader />
        <div className="bg-background">
          <Sidebar doClick={doToggle} toggle={toggle} />
          <div style={{ margin: marginLeft }} id="main-content">
            <div className="h-full flex-1 px-4 py-6 lg:px-8">
              {children}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
}
