import * as React from "react"
import { Metadata } from "next";

import { Separator } from "@/components/ui/separator";;

import { FormNewTicket } from "./components/form";
import "../styles.css"

import { siteConfig } from "@/config/site"

export const metadata: Metadata = {
  title: {
    default: "New Ticket",
    template: `%s - ${siteConfig.name}`,
  },
  description: "Create new ticket to STR o'House.",
}

export default async function NewTicketPage() {
  return (
    <>
      <div className="space-between space-y-4">
        <h2 className="text-3xl font-bold tracking-tight text-center sm:text-left">New Ticket</h2>
        <div>
          <p className="text-sm text-muted-foreground text-center sm:text-left">
            Hi! thank your for use our platform for communicating with Us.
          </p>
          <p className="text-sm text-muted-foreground text-center sm:text-left">
            Also please continue use this platform as main activity to Us.
          </p>
        </div>
      </div>
      
      <Separator className="my-4" />
      
      <div className="flex flex-col content-center space-y-4">

        <FormNewTicket />
        
      </div>
    </>
  )
}