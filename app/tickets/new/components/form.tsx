"use client"

import * as React from "react"
import { Metadata } from "next";
import { zodResolver } from "@hookform/resolvers/zod"
import { useFieldArray, useForm } from "react-hook-form"
import * as z from "zod"

import { Button } from "@/components/ui/button"
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form"
import { Input } from "@/components/ui/input"
import { Textarea } from "@/components/ui/textarea"

import { ComboboxService } from "./combobox-service";

const formSchema = z.object({
  category: z.string(),
  title: z.string().min(2).max(100),
  desc: z.string().min(50).max(500)
})

export const FormNewTicket = () => {

  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      category: "",
      title: "",
      desc: "",
    },
  })

  // 2. Define a submit handler.
  function onSubmit(values: z.infer<typeof formSchema>) {
    // Do something with the form values.
    // ✅ This will be type-safe and validated.
    console.log(values)
  }
  
  return <Form {...form}>
    <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-8">
      <FormField
        control={form.control}
        name="category"
        render={({ field }) => (
          <FormItem>
            <div className="grid gap-4 md:grid-cols-3">
              <FormLabel className="pt-2">Category</FormLabel>
              <div className="md:col-span-2">
                <FormControl>
                  <ComboboxService />
                </FormControl>
                <FormDescription>
                  Choose one for category ticket.
                </FormDescription>
                <FormMessage />
              </div>
            </div>
          </FormItem>
        )}
      />

      <FormField
        control={form.control}
        name="title"
        render={({ field }) => (
          <FormItem>
            <div className="grid gap-4 md:grid-cols-3">
              <FormLabel className="pt-2">Title</FormLabel>
              <div className="md:col-span-2">
                <FormControl>
                  <Input type="text" placeholder="Your title problem" {...field} />
                </FormControl>
                <FormDescription>
                  This is will be your ticket title.
                </FormDescription>
                <FormMessage />
              </div>
            </div>
          </FormItem>
        )}
      />

      <FormField
        control={form.control}
        name="desc"
        render={({ field }) => (
          <FormItem>
            <div className="grid gap-4 md:grid-cols-3">
              <FormLabel className="pt-2">Description</FormLabel>
              <div className="md:col-span-2">
                <FormControl>
                  <Textarea {...field} />
                </FormControl>
                <FormDescription>
                  Please tell use more about detail information so we can understand the problem.
                </FormDescription>
                <FormMessage />
              </div>
            </div>
          </FormItem>
        )}
      />
      <Button type="submit">Submit</Button>
    </form>
  </Form>
}