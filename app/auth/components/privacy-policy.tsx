"use client"

import * as React from "react"

import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog"

interface PrivacyPolicyProps extends React.HTMLAttributes<HTMLDivElement> { }

export function PrivacyPolicy({ className, ...props }: PrivacyPolicyProps) {
  return <>
    <Dialog>
      <DialogTrigger asChild>
        <a className="underline underline-offset-4 hover:text-primary" >
          Privacy Policy
        </a>
      </DialogTrigger>
      <DialogContent>
        <DialogHeader>
          <DialogTitle>Privacy Policy</DialogTitle>
          <DialogDescription>
            This action cannot be undone. Are you sure you want to permanently
            delete this file from our servers?
          </DialogDescription>
        </DialogHeader>
      </DialogContent>
    </Dialog>
  </>
}