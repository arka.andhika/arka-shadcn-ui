"use client"

import * as React from "react"

import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog"

interface TermsOfServiceProps extends React.HTMLAttributes<HTMLDivElement> {}

export function TermsOfService({ className, ...props }: TermsOfServiceProps) {
  return <>
    <Dialog>
      <DialogTrigger asChild>
        <a className="underline underline-offset-4 hover:text-primary" >
          Terms of Service
        </a>
      </DialogTrigger>
      <DialogContent>
        <DialogHeader>
          <DialogTitle>Terms of Service</DialogTitle>
          <DialogDescription>
            This action cannot be undone. Are you sure you want to permanently
            delete this file from our servers?
          </DialogDescription>
        </DialogHeader>
      </DialogContent>
    </Dialog>
  </>
}