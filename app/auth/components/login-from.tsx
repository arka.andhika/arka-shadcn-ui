"use client"

import * as React from "react"
import Link from "next/link"

import { cn } from "@/lib/utils"
import { Button } from "@/components/ui/button"
import { Input } from "@/components/ui/input"
import { Label } from "@/components/ui/label"
import { Icons } from "@/components/icons"

import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog"


interface LoginFormProps extends React.HTMLAttributes<HTMLDivElement> {}

export function LoginForm({ className, ...props }: LoginFormProps) {
  const [isLoading, setIsLoading] = React.useState<boolean>(false)

  async function onSubmit(event: React.SyntheticEvent) {
    event.preventDefault()
    setIsLoading(true)

    setTimeout(() => {
      setIsLoading(false)
    }, 3000)
  }

  return <>
    <Dialog>
      <DialogTrigger asChild>
        <Button variant="ghost" size="sm" className="absolute right-4 top-4 md:right-8 md:top-8">Login</Button>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[425px]">
        <DialogHeader>
          <DialogTitle>Login an account</DialogTitle>
          <DialogDescription>
            Make sure to input your valid email and password.
          </DialogDescription>
        </DialogHeader>
        <div className={cn("grid gap-6 mt-5", className)} {...props}>
          <form onSubmit={onSubmit}>
            <div className="grid gap-5">
              <div className="grid gap-1">
                <Label htmlFor="email">
                  Email
                </Label>
                <Input
                  id="email"
                  placeholder="name@example.com"
                  type="email"
                  autoCapitalize="none"
                  autoComplete="email"
                  autoCorrect="off"
                  disabled={isLoading}
                />
              </div>
              <div className="grid gap-1">
                <Label htmlFor="password">
                  Password
                </Label>
                <Input
                  id="password"
                  placeholder="********"
                  type="password"
                  autoCapitalize="none"
                  autoComplete="password"
                  autoCorrect="off"
                  disabled={isLoading}
                />
              </div>
              <div className="grid gap-1">
                <Button disabled={isLoading}>
                  {isLoading && (
                    // <Icons.spinner className="mr-2 h-4 w-4 animate-spin" />
                    "loading..."
                  )}
                  Login
                </Button>
                <div className="text-center">
                  <Link className="text-sm underline" href="/forgot-password">Forgot Password</Link>
                </div>
              </div>
            </div>
          </form>
        </div>
      </DialogContent>
    </Dialog>
  </>
}