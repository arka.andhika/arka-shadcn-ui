'use client';

import "@/styles/globals.css"

interface LoginLayoutProps {
  children: React.ReactNode,
}

export default function LoginLayout({ children }: LoginLayoutProps) {
  return <div className="relative flex min-h-screen flex-col">
    <div className="flex-1">
      {children}
    </div>
  </div>
}
