import { Metadata } from "next"

import { siteConfig } from "@/config/site"
import { fontWeather } from "@/lib/fonts";

import { Icons } from "@/components/icons"

import { RegisterForm } from "@/app/auth/components/register-form"
import { LoginForm } from "@/app/auth/components/login-from"
import { TermsOfService } from "@/app/auth/components/terms-of-service"
import { PrivacyPolicy } from "@/app/auth/components/privacy-policy"

export const metadata: Metadata = {
  title: {
    default: "Create an account",
    template: `%s - ${siteConfig.name}`,
  },
  description: "Create your account or do login to your account.",
}

export default function AuthenticationPage() {
  return (
    <>
      <div className="container relative h-screen flex-col items-center justify-center md:grid lg:max-w-none lg:grid-cols-2 lg:px-0">

        <LoginForm />

        <div className="relative hidden h-full flex-col bg-muted p-10 text-white dark:border-r lg:flex">
          <div
            className="absolute inset-0 bg-cover"
            style={{
              backgroundImage:
                "url(https://images.unsplash.com/photo-1590069261209-f8e9b8642343?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1376&q=80)",
            }}
          />
          <div className="relative z-20 flex items-center text-lg font-medium">
            <Icons.logo className="mr-2  h-6 w-6" />
            <span className="inline-block font-bold">{siteConfig.name}</span>
          </div>
          <div className="relative z-20 mt-auto">
            <blockquote className="space-y-2">
              <p className="text-lg">
                &ldquo;{siteConfig.quote.text}&rdquo;
              </p>
            </blockquote>
          </div>
        </div>
        <div className="pt-20 md:pt-0 lg:p-8">
          <div className="mx-auto flex w-full flex-col justify-center space-y-6 sm:w-[350px]">
            <div className="flex flex-col space-y-2 text-center">
              <h1 className="text-2xl font-semibold tracking-tight">
                Create an account
              </h1>
              <p className="text-sm text-muted-foreground">
                Enter your email below to create your account
              </p>
            </div>
            <RegisterForm />
            <p className="px-8 text-center text-sm text-muted-foreground">
              By clicking continue, you agree to our{" "}
              <TermsOfService />
              {" "}and{" "}
              <PrivacyPolicy />
              .
            </p>
          </div>
        </div>
      </div>
    </>
  )
}