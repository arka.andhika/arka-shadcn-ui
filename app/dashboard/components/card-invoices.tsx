
import Link from "next/link";

import { ExternalLink, ScrollText } from "lucide-react"
import {
  Card,
  CardContent,
  CardHeader,
  CardTitle,
} from "@/components/ui/card"

export const CardInvoices = () => {
  return <Card>
    <CardHeader className="flex flex-row items-center justify-between space-y-0 pb-2">
      <div className="flex flex-row">
        <ScrollText className="mr-2 h-4 w-4" />
        <CardTitle className="text-sm font-medium">
          Total Invoices
        </CardTitle>
      </div>
      <Link href={'/invoices'}>
        <ExternalLink className="h-4 w-4 text-muted-foreground" />
      </Link>
    </CardHeader>
    <CardContent>
      <div className="flex justify-between">
        <div>
          <div className="text-2xl font-bold">20</div>
          <p className="text-xs text-muted-foreground">
            $4M amount total invoices
          </p>
          <p className="text-xs text-muted-foreground">
            $1M amount total paid invoices
          </p>
        </div>
        
        <div>
          <p className="text-xs text-muted-foreground">
            1 Paid
          </p>
          <p className="text-xs text-muted-foreground">
            1 Unpaid
          </p>
          <p className="text-xs text-muted-foreground">
            1 Canceled
          </p>
          <p className="text-xs text-muted-foreground">
            1 Refund
          </p>
        </div>
      </div>
    </CardContent>
  </Card>
}