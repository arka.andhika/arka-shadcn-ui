

import Link from "next/link";

import { ExternalLink } from "lucide-react"
import {
  Card,
  CardContent,
  CardDescription,
  CardHeader,
  CardTitle,
} from "@/components/ui/card"
import { Separator } from "@/components/ui/separator"

import { listActivities } from "@/config/activity";
import monthToString from "@/lib/month-to-string";

export const CardActivities = () => {
  return <Card>
    <CardHeader className="pb-2">
      <div className="flex flex-row items-center justify-between space-y-0">
        <CardTitle className="text-2xl">
          Activities
        </CardTitle>
        <Link href={'/activities'}>
          <ExternalLink className="h-4 w-4 text-muted-foreground" />
        </Link>
      </div>
      <CardDescription>
        All activity of your account and member.
      </CardDescription>
    </CardHeader>
    <CardContent>
      <Separator className="my-4" />
      <div className="space-y-1">
        {listActivities?.length ? listActivities?.map(
          (activity, i) => <div key={i} className="flex justify-between">
            <p className="text-xs">{activity.name}</p>
            <p className="text-xs text-muted-foreground">
              {monthToString(activity.date.getMonth())}
              {" "}
              {(activity.date.getDate()).toString()}
            </p>
          </div>
        ) : <></>}
      </div>
    </CardContent>
  </Card>
}