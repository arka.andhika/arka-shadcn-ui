
import Link from "next/link";

import { ExternalLink, Ticket } from "lucide-react"
import {
  Card,
  CardContent,
  CardHeader,
  CardTitle,
} from "@/components/ui/card"

export const CardTickets = () => {
  return <Card>
    <CardHeader className="flex flex-row items-center justify-between space-y-0 pb-2">
      <div className="flex flex-row">
        <Ticket className="mr-2 h-4 w-4" />
        <CardTitle className="text-sm font-medium">
          Total Tickets
        </CardTitle>
      </div>
      <Link href={'/tickets'}>
        <ExternalLink className="h-4 w-4 text-muted-foreground" />
      </Link>
    </CardHeader>
    <CardContent>
    <div className="flex justify-between">
        <div>
          <div className="text-2xl font-bold">20</div>
          <p className="text-xs text-muted-foreground">
            20% case close
          </p>
          <p className="text-xs text-muted-foreground">
            Avg. answering Tickets in 5 hours
          </p>
        </div>
        
        <div>
          <p className="text-xs text-muted-foreground">
            1 Open
          </p>
          <p className="text-xs text-muted-foreground">
            1 Replied
          </p>
          <p className="text-xs text-muted-foreground">
            1 Close
          </p>
        </div>
      </div>
    </CardContent>
  </Card>
}