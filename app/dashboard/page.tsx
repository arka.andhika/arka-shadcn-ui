import * as React from "react"
import { Metadata } from "next";
import Link from "next/link";

import { Separator } from "@/components/ui/separator"
import { Tabs, TabsContent, TabsList, TabsTrigger } from "@/components/ui/tabs"
import { Button } from "@/components/ui/button"

import { PlusCircle } from "lucide-react"
import { CardInvoices } from "./components/card-invoices";
import { CardTickets } from "./components/card-tickets";
import { CardActivities } from "./components/card-activity";
import { columns } from "./components/consultations/columns"
import { DataTable } from "./components/consultations/data-table"
import { getConsultations } from "@/config/consultations"
import "./styles.css"

import { siteConfig } from "@/config/site"

export const metadata: Metadata = {
  title: {
    default: "Dashboard",
    template: `%s - ${siteConfig.name}`,
  },
  description: "Home page of STR o'House website.",
}

export default async function DashboardPage() {
  const data = await getConsultations();

  return (
    <>
      <div className="space-between flex flex-col sm:flex-row items-center space-y-4">
        <h2 className="text-3xl font-bold tracking-tight">Dashboard</h2>
        <div className="sm:ml-auto sm:mr-4">
          <Link href={'/consultations/new'}>
            <Button>
              <PlusCircle className="mr-2 h-4 w-4" />
              New Consultation
            </Button>
          </Link>
        </div>
      </div>
      <Tabs defaultValue="main" className="space-y-4">
        <div className="flex items-center">
          <TabsList>
            <TabsTrigger value="main" className="relative">
              Main
            </TabsTrigger>
            <TabsTrigger value="consultations">
              Consultations
            </TabsTrigger>
          </TabsList>
        </div>
        <TabsContent
          value="main"
          className="border-none p-0 outline-none space-y-4"
        >
          <div className="grid gap-4 lg:grid-cols-2 2xl:grid-cols-3">
            <div className="2xl:col-span-2">
              <div className="mt-6 space-y-1">
                <h2 className="text-2xl font-semibold tracking-tight">
                  Welcome back
                </h2>
                <p className="text-sm text-muted-foreground">
                  Summary of your invoice & ticket.
                </p>
              </div>
              <Separator className="my-4" />
              <div className="space-y-4">
                <CardInvoices />
                <CardTickets />
              </div>
            </div>
            <div>
              <div className="mt-6 space-y-1">
                <CardActivities />
              </div>
            </div>
          </div>
        </TabsContent>
        <TabsContent
          value="consultations"
          className="h-full flex-col border-none p-0 data-[state=active]:flex"
        >
          <div className="flex items-center justify-between">
            <div className="space-y-1">
              <h2 className="text-2xl font-semibold tracking-tight">
                List Consultations
              </h2>
              <p className="text-sm text-muted-foreground">
                Your data of consultations to Us.
              </p>
            </div>
          </div>
          <Separator className="my-4" />
          <DataTable columns={columns} data={data} />
        </TabsContent>
      </Tabs>
    </>
  )
}