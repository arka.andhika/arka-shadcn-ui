import monthToString from "./month-to-string";
import dtToTime from "./timestamp-to-time";

export const dateTime = (inputDate: Date) :string => {
  const date = `${inputDate.getDate()} ${monthToString(inputDate.getMonth())} ${inputDate.getFullYear()}`;
  const time = dtToTime(inputDate);

  return `${date} ${time}`
}