
export default function monthToString(month: number): string {
  const strMonths = [
    'Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Oct', 'Nov', 'Des'
  ];
  return strMonths[month];
}