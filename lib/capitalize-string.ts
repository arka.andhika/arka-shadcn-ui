
export const capitalize = ( val: string ) => val.charAt(0).toUpperCase() + val.slice(1);

export const capStrings = ( vals: string[] ) => vals.map(val => capitalize(val));

export const capWord = ( word: string ) => word.split(' ').map(letter => capitalize(letter));

export const symWord = ( word: string ) => word.split(' ').map(letter => letter.charAt(0).toUpperCase()).join();