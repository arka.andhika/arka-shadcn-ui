export default function dtToTime (date: Date): string {
  return `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`
}