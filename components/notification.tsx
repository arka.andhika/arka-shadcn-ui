"use client"

import Link from "next/link"
import { BellRing } from "lucide-react"
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuGroup,
  DropdownMenuItem,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu"
import { Button } from "@/components/ui/button";

import { listNotif } from "../config/notif"
import monthToString from "@/lib/month-to-string";

const strTruncate = (str: string, n: number) => {
  if (str.length <= n) { return str; }
  const subString = str.slice(0, n-1); // the original check
  return subString + "...";
};

export const Notification = () => {
  return (
    <>
      <Link href={'/notification'}>
        <Button variant="ghost" size="sm" className="md:hidden">
          <BellRing />
          <span className="sr-only">Notification</span>
        </Button>
      </Link>
      <DropdownMenu>
        <DropdownMenuTrigger asChild className="hidden md:block">
          <Button variant="ghost" size="sm">
            <BellRing />
            <span className="sr-only">Notification</span>
          </Button>
        </DropdownMenuTrigger>
        <DropdownMenuContent className="w-56" align="end" forceMount>
          <DropdownMenuGroup>
            {listNotif?.map((notification, i) => {
              if (i > 5) return <></>

              return <Link href={notification.link} key={i}>
                <DropdownMenuItem>
                  <div>
                    <small>
                      {notification.category}
                      {" - "}
                      {monthToString(notification.date.getMonth())}
                      {" "}
                      {(notification.date.getDate()).toString()}
                    </small>
                    <p className="font-bold">{notification.title}</p>
                    <p>{strTruncate(notification.slug, 25)}</p>
                  </div>
                </DropdownMenuItem>
                <DropdownMenuSeparator />
              </Link>
            })}
          </DropdownMenuGroup>
          <DropdownMenuGroup>
            <div className="flex justify-between p-2">
              <small>Mark as read</small>
              <Link href={'/notification'}>
                <small>See all</small>
              </Link>
            </div>
          </DropdownMenuGroup>
        </DropdownMenuContent>
      </DropdownMenu>
    </>
  )
}