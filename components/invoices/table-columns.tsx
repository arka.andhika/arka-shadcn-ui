"use client"

import Link from "next/link"

import { ColumnDef } from "@tanstack/react-table"
import { ArrowUpDown, MoreHorizontal } from "lucide-react"

import { Badge } from "@/components/ui/badge"
import { Button } from "@/components/ui/button"
import { Checkbox } from "@/components/ui/checkbox"
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu"

import { invoice } from "@/config/invoice"
import monthToString from "@/lib/month-to-string"

export const columnsInvoices: ColumnDef<invoice>[] = [
  {
    id: "select",
    header: ({ table }) => (
      <Checkbox
        checked={table.getIsAllPageRowsSelected()}
        onCheckedChange={(value) => table.toggleAllPageRowsSelected(!!value)}
        aria-label="Select all"
      />
    ),
    cell: ({ row }) => (
      <Checkbox
        checked={row.getIsSelected()}
        onCheckedChange={(value) => row.toggleSelected(!!value)}
        aria-label="Select row"
      />
    ),
    enableSorting: false,
    enableHiding: false,
  },
  {
    accessorKey: "number",
    header: "No.",
    cell: ({ row }) => {
      const invoice = row.original;
 
      return <Button variant="secondary" disabled={true}>{invoice.number}</Button>
    },
  },
  {
    accessorKey: "createdat",
    header: ({ column }) => {
      return (
        <Button
          variant="ghost"
          onClick={() => column.toggleSorting(column.getIsSorted() === "asc")}
        >
          Created Date
          <ArrowUpDown className="ml-2 h-4 w-4" />
        </Button>
      )
    },
    cell: ({ row }) => {
      const invoice = row.original;
      const createDate = new Date(invoice.createdat);
      const date = `${createDate.getDate()} ${monthToString(createDate.getMonth())} ${createDate.getFullYear()}`;
      const time = `${createDate.getHours()}:${createDate.getMinutes()}:${createDate.getSeconds()}`;
 
      return <div>{date} - {time}</div>
    },
  },
  {
    accessorKey: "duedate",
    header: ({ column }) => {
      return (
        <Button
          variant="ghost"
          onClick={() => column.toggleSorting(column.getIsSorted() === "asc")}
        >
          Due Date
          <ArrowUpDown className="ml-2 h-4 w-4" />
        </Button>
      )
    },
    cell: ({ row }) => {
      const invoice = row.original;
      const dueDate = new Date(invoice.duedate);
      const date = `${dueDate.getDate()} ${monthToString(dueDate.getMonth())} ${dueDate.getFullYear()}`;
      const time = `${dueDate.getHours()}:${dueDate.getMinutes()}:${dueDate.getSeconds()}`;
 
      return <div>{date} - {time}</div>
    },
  },
  {
    accessorKey: "status",
    header: ({ column }) => {
      return (
        <Button
          variant="ghost"
          onClick={() => column.toggleSorting(column.getIsSorted() === "asc")}
        >
          Status
          <ArrowUpDown className="ml-2 h-4 w-4" />
        </Button>
      )
    },
    cell: ({ row }) => {
      const invoice = row.original;
      const status = invoice.status.charAt(0).toUpperCase() + invoice.status.slice(1);
 
      return <Badge variant="secondary">{status}</Badge>
    },
  },
  {
    id: "actions",
    cell: ({ row }) => {
      const invoice = row.original
 
      return (
        <DropdownMenu>
          <DropdownMenuTrigger asChild>
            <Button variant="ghost" className="h-8 w-8 p-0">
              <span className="sr-only">Open menu</span>
              <MoreHorizontal className="h-4 w-4" />
            </Button>
          </DropdownMenuTrigger>
          <DropdownMenuContent align="end">
            <DropdownMenuLabel>Actions</DropdownMenuLabel>
            <DropdownMenuItem>
              <Link href={`/invoices/${invoice.number}`}>View Detail</Link>
            </DropdownMenuItem>
            <DropdownMenuItem>Close</DropdownMenuItem>
          </DropdownMenuContent>
        </DropdownMenu>
      )
    },
  },
]
