import * as React from "react"
import Link from "next/link"

import {
  ScrollText,
  Recycle,
  Paperclip,
  FileClock,
  Ticket,
  Boxes,
  Stethoscope,
  Home,
  ArrowBigLeftDash,
  ArrowBigRightDash,
  Cpu,
  BookKey,
  Atom,
} from "lucide-react"

import { Separator } from "@/components/ui/separator"
import { Button } from "@/components/ui/button"
import { ScrollArea } from "@/components/ui/scroll-area"

import { siteConfig } from "@/config/site"

type sidebarProp = {
  toggle: boolean,
  doClick: React.Dispatch<React.SetStateAction<boolean>>
}

export function Sidebar({ toggle, doClick }: sidebarProp) {

  const [width, setWidth] = React.useState<string>('212');
  const [iconmr, setIconmr] = React.useState<string>('2');

  React.useEffect(() => {
    setWidth(toggle ? '212' : '75');
    setIconmr(toggle ? '2' : '0');
  }, [toggle]);

  return (
    <div style={{ width: `${width}px` }} className={`fixed bg-background top-[4rem] h-screen w-[${width}px] border-r py-4 z-30`}>
      <ScrollArea className={`h-[calc(100vh-9rem)] flex flex-col justify-between`}>

        <div className="md:hidden">
          <div className="px-4 py-2">
            <div className="space-y-1">
              <Link href="#">
                <Button
                  variant="ghost"
                  size="sm"
                  className="w-full justify-start"
                >
                  <BookKey className={`mr-${iconmr} h-4 w-4`} />
                  {toggle && <>Documentation</>}
                </Button>
              </Link>
              <Link href="#">
                <Button
                  variant="ghost"
                  size="sm"
                  className="w-full justify-start"
                >
                  <Atom className={`mr-${iconmr} h-4 w-4`} />
                  {toggle && <>Blog</>}
                </Button>
              </Link>
            </div>
          </div>

          <Separator className="my-4" />
        </div>

        <div className="px-4 py-2">
          <div className="space-y-1">
            <Link href={'/dashboard'}>
              <Button
                variant="secondary"
                size="sm"
                className="w-full justify-start"
              >
                <Home className={`mr-${iconmr} h-4 w-4`} />
                {toggle && <>Dashboard</>}
              </Button>
            </Link>
            <Link href={'/invoices'}>
              <Button
                variant="ghost"
                size="sm"
                className="w-full justify-start"
              >
                <ScrollText className={`mr-${iconmr} h-4 w-4`} />
                {toggle && <>Invoices</>}
              </Button>
            </Link>
            <Link href={'/tickets'}>
              <Button
                variant="ghost"
                size="sm"
                className="w-full justify-start"
              >
                <Ticket className={`mr-${iconmr} h-4 w-4`} />
                {toggle && <>Tickets</>}
              </Button>
            </Link>
          </div>
        </div>

        <Separator className="my-4" />
      </ScrollArea>

      <div style={{ width: `${width}px` }} className="fixed bottom-0 px-4 py-2">
        <Button variant="secondary" className="w-full px-0" onClick={() => doClick(toggle !== true)}>
          {toggle 
            ? <ArrowBigLeftDash className={`mr-${iconmr} h-4 w-4`} /> 
            : <ArrowBigRightDash className={`mr-${iconmr} h-4 w-4`} />
          }
        </Button>
      </div>
    </div>
  )
}