
import { Badge } from "@/components/ui/badge"

import monthToString from "@/lib/month-to-string";

const dtToTime = (date: Date) => {
  return `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`
}

interface date {
  date: Date
  label?: string
}

export const BadgeDate = ({ date, label }: date) => {
  return <Badge variant="secondary" className="mb-5 py-2 px-5">
    {label && <>{label}: {" "}</>}
    {monthToString(date.getMonth())} {" "}
    {(date.getDate()).toString()} {" "}
    {dtToTime(date)}
  </Badge>
}

export const BadgeDateSmall = ({ date, label }: date) => {
  return <Badge variant="secondary">
    {label && <>{label}: {" "}</>}
    {monthToString(date.getMonth())} {" "}
    {(date.getDate()).toString()} {" "}
    {dtToTime(date)}
  </Badge>
}