
interface items {
  items: string[]
}

export const Breadcrumbs = ({ items }: items) => {
  return <div className="mb-4 flex items-center space-x-1 text-sm text-muted-foreground">
    {items.map((item, i) => {
      if (i + 1 !== items.length) {
        return <>
          <div className="overflow-hidden text-ellipsis whitespace-nowrap">{item}</div>
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="h-4 w-4"><polyline points="9 18 15 12 9 6"></polyline></svg>
        </>
      }

      return <div className="font-medium text-foreground">
        {item}
      </div>
    })}
  </div>
}